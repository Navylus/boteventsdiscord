const Discord = require('discord.js')

const client = new Discord.Client()

//const config = require('./config.json')
const aws = require('aws-sdk');

let s3 = new aws.S3({
  token: process.env.TOKEN,
  prfix: process.env.PREFIX
});

//let events = [] //PROD MODE ONLY
let events = require('./mock.json') //DEV MODE ONLY

client.on('ready', () => {
  console.log(`Bot has started.`)
})

client.on('message', async message => {
  if (message.author.bot) return

  if (message.content.indexOf(s3.prefix) !== 0) return
  const args = message.content
    .slice(s3.prefix.length)
    .trim()
    .split(/ +/g)

  const command = args.shift().toLowerCase()

  if (command === 'ping') {
    const m = await message.channel.send('Ping?')
    m.edit(
      `Latency is ${m.createdTimestamp -
        message.createdTimestamp}ms. API Latency is ${Math.round(
        client.ping
      )}ms`
    )
  }

  if (command === 'newevent') {
    //Format : --newevent nom ville nbPlaces jour heure
    //ie: --newevent Evo Saulnes 14 12/09/1997 21:00
    if (args.length !== 5) {
      return message.reply(
        `Désolé utilise le format suivant: --newevent nom ville nbPlaces jour heure`
      )
    }
    const nom = args[0]
    const ville = args[1]
    const nbPlaces = args[2]
    const jour = args[3]
    const heure = args[4]
    const newEvent = {
      nom: nom,
      ville: ville,
      nbPlaces: nbPlaces,
      placePrises: 0,
      jour: jour,
      admin: message.author.id,
      createur: message.author.username,
      heure: heure,
      participants: []
    }
    events.push(newEvent)
    const newMessage = `**nouvel évenement crée :**\nnom: ${nom} \nville: ${ville} \nnombre de places: ${nbPlaces} \njour: ${jour} \nheure: ${heure} \ncréateur: ${
      message.author.username
    }`
    message.channel.send(newMessage)
  }

  if (command === 'deleteevent') {
    //Format : --deleteevent nomEvenement nomEvenemnt
    //ie: --deleteevent Evo3 Evo3
    if (args.length < 2) {
      return message.reply(
        'Désolé utilise le format suivant: --deleteevent nomEvenement nomEvenemnt'
      )
    }
    const nom = args[0]
    const nom2 = args[1]
    if (nom !== nom2) {
      return message.reply(
        `Si vous voullez supprimer votre évenement entrez deux fois le nom de l'évenement pour confirmer votre choix`
      )
    }
    for (var i in events) {
      const event = events[i]
      if (event.nom === args[0]) {
        if (message.author.id !== event.admin) {
          return message.reply(
            `Désolé seul le créateur de l'évenement ou un admin peut supprimer cet évenement.`
          )
        }
        delete events[i]
      }
    }
    const newMessage = `Votre évenement ${nom} à été supprimé`
    message.channel.send(newMessage)
  }

  if (command === 'participer') {
    //Format : --participer nomDeLEvenement
    //ie: --participer Evo
    if (args.length < 1) {
      return message.reply(
        'Désolé utilise le format suivant: --participer nomDeLEvenement'
      )
    }
    for (var i in events) {
      const event = events[i]
      if (event.nom === args[0]) {
        if (event.placePrises == event.nbPlaces) {
          return message.reply(
            `Désolé l'évenement est remplis, tu pourras participer une prochaine fois!`
          )
        }
        if (event.participants.includes(message.author.username)) {
          return message.reply(`Désolé tu es déjà inscris, on se vois bientôt!`)
        }
        event.participants.push(message.author.username)
        event.placePrises = events[i].placePrises + 1
        return message.reply(
          `tu es inscris à l'évenement *${
            event.nom
          }*. Nombre de places prises pour la session: ${event.placePrises}/${
            event.nbPlaces
          }`
        )
      }
    }
    return message.reply(`Désolé l'évenement n'existe pas`)
  }

  if (command === 'listeevents') {
    //Format : --listeevents
    //ie: --listeevents
    let res = ''
    for (var i in events) {
      const event = events[i]
      res += `\n\n**${event.nom}** \nville: ${
        event.ville
      } \nnombre de places: ${event.placePrises}/${event.nbPlaces} \njour: ${
        event.jour
      } \nheure: ${event.heure} \ncréateur: ${event.createur} \nparticipants: ${
        event.participants
      }`
    }
    return message.reply(
      `${res} \n\nPour plus de renseignements tapez --eventshelp`
    )
  }

  if (command === 'annulerinscription') {
    //Format : --annulerinscription nomDeLEvenement
    //ie: --annulerinscription Evo
    if (args.length < 1) {
      return message.reply(
        'Désolé utilise le format suivant: --annulerinscription nomDeLEvenement'
      )
    }
    for (var i in events) {
      const event = events[i]
      if (event.nom === args[0]) {
        if (!event.participants.includes(message.author.username)) {
          return message.reply(
            `Désolé tu n'es pas inscris, tu ne peux pas te désinscrire.`
          )
        }
        if (event.participants.includes(message.author.username)) {
          const index = event.participants.indexOf(message.author.username)
          if (index != -1) {
            event.participants.splice(index, 1)
          }
          event.placePrises = event.placePrises - 1
          return message.reply(`Tu n'est plus inscris à ${event.nom}`)
        }
      }
    }
    return message.reply(`Désolé l'évenement n'existe pas`)
  }

  if (command === 'eventshelp') {
    return message.reply(
      `
      Format : --listeevents
      Exemple d'utilisation : --listeevents
      Kézako : Pour obtenir la liste complète des évenements, le nombre de places restantes et les participants.

      Format : --newevent nom ville nbPlaces jour heure
      Exemple d'utilisation : --newevent Evo Las-Vegas 14 13/07/2018 14:00
      Kézako : La commande pour créer ton évenement, seul toi et les admins pourront supprimer et modifier cet évenement.

      Format : --participer nomDeLEvenement
      Exemple d'utilisation : --participer Evo
      Kézako : La commande pour s'inscrire à un évenement, regarde le nom de l'évenement qui t'interesse avant de t'inscrire!  
      
      Format : --deleteevent nomEvenement nomEvenemnt
      Exemple d'utilisation : --deleteevent Evo Evo
      Kézako : La commande pour le créateur d'un évenement ou pour les admins qui sert à supprimer un évenement, tape 2 fois le nom pour ne pas te tromper et supprimer un évenement sans le vouloir.
      
      Format : --annulerinscription nomEvenement
      Exemple d'utilisation : --annulerinscription Evo
      Kézako : La commande te désinscrire d'un évenement
    
      Crée par Navylus
      gitLab link : https://gitlab.com/Navylus/boteventsdiscord
      `
    )
  }
})

client.login(s3.token)
